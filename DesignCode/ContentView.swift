//
//  ContentView.swift
//  DesignCode
//
//  Created by Antonín Šimek on 23/09/2020.
//  Copyright © 2020 Antonín Šimek. All rights reserved.
//

import SwiftUI

struct ContentView: View {
	@State var show = false
	@State var viewState = CGSize.zero
	@State var showCard = false
	
	var body: some View {
		ZStack {
			TitleView()
				.blur(radius: show ? 20 : 0)
				.opacity(showCard ? 0.4 : 1)
				.offset(y: showCard ? -200 : 0)
				.animation(
					Animation
						.default
						.delay(0.1)
			)
			
			BackCardView()
				.frame(width: 340, height: 220)
				.background(show ? Color("card4") : Color("card3"))
				.cornerRadius(20)
				.shadow(radius: 20)
				.offset(x: 0, y: show ? 0 : -40)
				.offset(x: viewState.width, y: viewState.height)
				.offset(y: showCard ? -180 : 0)
				.scaleEffect(0.9)
				.rotationEffect(.degrees(show ? 0 : 10))
				.rotationEffect(.degrees(showCard ? -10 : 0))
				.rotation3DEffect(.degrees(10), axis: (x: 10, y: 0, z: 0))
				.blendMode(.hardLight)
				.animation(.easeInOut(duration: 0.5))
			
			BackCardView()
				.frame(width: 340, height: 220)
				.background(show ? Color("card3") : Color("card4"))
				.cornerRadius(20)
				.shadow(radius: 20)
				.offset(x: 0, y: show ? 0 : -20)
				.offset(x: viewState.width, y: viewState.height)
				.offset(y: showCard ? -140 : 0)
				.scaleEffect(0.95)
				.rotationEffect(.degrees(show ? 0 : 5))
				.rotationEffect(.degrees(showCard ? -5 : 0))
				.rotation3DEffect(.degrees(5), axis: (x: 10, y: 0, z: 0))
				.blendMode(.hardLight)
				.animation(.easeInOut(duration: 0.3))
			
			CardView()
				.frame(width: showCard ? 375 : 340, height: 220)
				.background(Color.black)
//				.cornerRadius(20)
				.clipShape(RoundedRectangle(cornerRadius: showCard ? 30 : 20, style: .continuous))
				.shadow(radius: 20)
				.offset(x: viewState.width, y: viewState.height)
				.offset(y: showCard ? -100 : 0)
				.blendMode(.hardLight)
				.animation(.spring(response: 0.3, dampingFraction: 0.6, blendDuration: 0))
				.onTapGesture {
					self.showCard.toggle()
			}
			.gesture(
				showCard ? nil : DragGesture()
				.onChanged { value in
					self.viewState = value.translation
					self.show = true
				}
				.onEnded { value in
					self.viewState = .zero
					self.show = false
				}
			)
			
			BottomCardView()
				.offset(x: 0, y: showCard ? 300 : 1000)
				.blur(radius: show ? 20 : 0)
				.animation(.timingCurve(0.2, 0.8, 0.2, 1, duration: 0.8))
		}
	}
}

struct CardView: View {
	var body: some View {
		VStack {
			HStack {
				VStack(alignment: .leading) {
					Text("UI Design")
						.font(.title)
						.fontWeight(.semibold)
						.foregroundColor(.white)
					Text("Certificate")
						.foregroundColor(Color("accent"))
				}
				Spacer()
				Image("Logo1")
			}
			.padding(.horizontal, 20)
			.padding(.top, 20)
			Spacer()
			Image("Card1")
				.resizable()
				.aspectRatio(contentMode: .fill)
				.frame(width: 300, height: 110, alignment: .top)
		}
	}
}

struct BackCardView: View {
	var body: some View {
		VStack {
			Spacer()
		}
	}
}

struct TitleView: View {
	var body: some View {
		VStack {
			HStack {
				Text("Certificates")
					.font(.title)
					.fontWeight(.bold)
				Spacer()
			}
			.padding()
			Image("Background1")
			Spacer()
		}
	}
}

struct BottomCardView: View {
	var body: some View {
		VStack(spacing: 20) {
			Rectangle()
				.frame(width: 40, height: 5)
				.cornerRadius(3)
				.opacity(0.1)
			Text("This certificate is proof that Ľubica has easily achieved and completed this SwiftUI course.")
				.multilineTextAlignment(.center)
				.lineSpacing(4)
				.font(.subheadline)
			Spacer()
		}
		.padding(.top, 8)
		.padding(.horizontal, 20)
		.frame(maxWidth: .infinity)
		.background(Color.white)
		.cornerRadius(30)
		.shadow(radius: 20)
	}
}

struct ContentView_Previews: PreviewProvider {
	static var previews: some View {
		Group {
//			ContentView()
//				.previewDevice("iPhone SE (1st generation)")
//				.previewDisplayName("iP SE")
//			ContentView()
//				.previewDevice("iPhone 8")
//				.previewDisplayName("iP 8")
			ContentView()
				.previewDevice("iPhone 11 Pro")
				.previewDisplayName("iP 11 P")
		}
	}
}
